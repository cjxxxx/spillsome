# README #

Spill Some: The Blood Tool is a launcher and demo utility for Blood/Cryptic Passage written to run under DOS/Dosbox. The source is provided under the MIT License.  

### What is this repository for? ###

* Spill Some: The Blood Tool
* Version 1.0

### How do I get set up? ###

* Spill Some is intended to be built using Turbo C v2.01 under an MS-DOS compatible OS or Dosbox.
* Move the resulting binary(SPILL.EXE) to your Blood directory.
* Applicable to Dosbox only: move blood_cd.cue and dummy_cd.bin to your Blood directory, assuming you have the .ogg CD music rips installed in /blood/music. 

### Contribution guidelines ###

* This software is complete. No contributions are needed. Feel free to fork your own version, however. 

### Who do I talk to? ###

* Repo owner